const Jimp = require('jimp');

const { expect } = require('chai');
const { describe, it } = require('mocha');
const { getAllRGBValues } = require('../../helpers/getAllRGBValues');

describe('The getAllRGBValues function', () => {
  it('Should return all RGB values from an image', () => {
    const IMAGE = new Jimp(1, 5, 'cornflowerblue');

    const RESULT = getAllRGBValues(IMAGE);
    const EXPECTED = ['100', '149', '237'];

    expect(RESULT).to.eql(EXPECTED);
  });
});
