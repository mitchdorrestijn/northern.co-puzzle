const { expect } = require('chai');
const { describe, it } = require('mocha');
const { getAllASCIICharacters } = require('../../helpers/getAllASCIICharacters');

describe('The getAllASCIICharacters function', () => {
  it('Should return all ASCII characters from an integer array', () => {
    const INTEGER_ARRAY = [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100];
    const RESULT = getAllASCIICharacters(INTEGER_ARRAY);
    const EXPECTED = ['H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'];

    expect(RESULT).to.eql(EXPECTED);
  });
});
