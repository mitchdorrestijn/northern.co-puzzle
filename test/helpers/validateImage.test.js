const Jimp = require('jimp');

const { expect } = require('chai');
const { describe, it } = require('mocha');
const { validateImage } = require('../../helpers/validateImage');

const createImage = (width, height) => {
  const image = new Jimp(width, height, 'cornflowerblue');
  image.getHeight = () => image.bitmap.height;
  return image;
};

describe('The validateImage function', () => {
  it('Should not throw an error if the image has a height of 1 pixel', () => {
    const IMAGE = createImage(5, 1);
    const RESULT = validateImage(IMAGE);
    expect(RESULT).to.not.throw; // eslint-disable-line no-unused-expressions
  });

  it('Should throw an error if the image is higher than 1 pixel', async () => {
    const IMAGE = createImage(5, 2);
    expect(() => validateImage(IMAGE)).to.throw('Oh snap! The height of the image can only have a height of 1 pixels in order for this script to work.');
  });
});
