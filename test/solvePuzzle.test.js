const path = require('path');
const { expect } = require('chai');
const { describe, it } = require('mocha');
const { solvePuzzle } = require('../solvePuzzle');

describe('The solvePuzzle function', () => {
  it('Should return the correct solution of the puzzle', async () => {
    const IMAGE_URL = path.resolve(__dirname, './testImage.png');

    const RESULT = await solvePuzzle(IMAGE_URL);
    const EXPECTED = 'hello world!';

    expect(RESULT).to.equal(EXPECTED);
  });
});
