const Jimp = require('jimp');

/**
 * Gets all the RGB values of the image
 * @param {Object} IMAGE The image object created by Jimp
 * @returns {Array} A array containing the integers as a String
 */
const getAllRGBValues = (IMAGE) => {
  let allRGBValues = [];
  for (let i = 0; i < IMAGE.getWidth(); i += 1) {
    const RGBAValuesOfPixel = Jimp.intToRGBA(IMAGE.getPixelColor(i, 1));
    const RGBValuesOfPixelsAnArray = Object.values(RGBAValuesOfPixel).splice(0, 3);

    allRGBValues = [...allRGBValues, ...RGBValuesOfPixelsAnArray];
  }

  return allRGBValues.map(String);
};

module.exports = {
  getAllRGBValues,
};
