const VALIDATION_ERRORS = {
  MAX_HEIGHT_ALLOWENCE_ERROR: 'Oh snap! The height of the image can only have a height of %MAX_IMAGE_HEIGHT_PX pixels in order for this script to work.',
};

/**
 * Validates the height of the image
 * @param {Object} IMAGE The image object created by Jimp
 * @throws Will throw an error if the height of the image is greater than 0px
 * @returns {void}
 */
const validateImage = (IMAGE) => {
  const MAX_IMAGE_HEIGHT = 1;
  if (IMAGE.getHeight() > MAX_IMAGE_HEIGHT) {
    throw new Error(VALIDATION_ERRORS.MAX_HEIGHT_ALLOWENCE_ERROR.replace('%MAX_IMAGE_HEIGHT_PX', MAX_IMAGE_HEIGHT));
  }
};

module.exports = {
  validateImage,
};
