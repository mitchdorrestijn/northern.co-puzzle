/**
 * Gets all the ASCII characters from an integer array
 * @param {Array} INTEGER_ARRAY An array containing integers as a String
 * @returns {Array} An array containing the characters that matched the integers
 */
const getAllASCIICharacters = (INTEGER_ARRAY) => {
  const allASCIICharacters = [];
  INTEGER_ARRAY.forEach((character) => {
    allASCIICharacters.push(String.fromCharCode(character));
  });

  return allASCIICharacters;
};

module.exports = {
  getAllASCIICharacters,
};
