const { solvePuzzle } = require('./solvePuzzle');

(async () => {
  const IMAGE_URL = 'https://www.northern.co/wp-content/themes/northern/images/puzzle.png';

  const solution = await solvePuzzle(IMAGE_URL);
  console.log(solution); // eslint-disable-line no-console
})().catch((error) => {
  console.log(error); // eslint-disable-line no-console
});
