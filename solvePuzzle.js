const Jimp = require('jimp');
const { validateImage } = require('./helpers/validateImage');
const { getAllRGBValues } = require('./helpers/getAllRGBValues');
const { getAllASCIICharacters } = require('./helpers/getAllASCIICharacters');

const solvePuzzle = async (IMAGE_URL) => {
  const IMAGE = await Jimp.read(IMAGE_URL);
  validateImage(IMAGE);

  const allRGBValues = getAllRGBValues(IMAGE);
  const allASCIICharacters = getAllASCIICharacters(allRGBValues);
  const solution = allASCIICharacters.join('');

  return solution;
};

module.exports = {
  solvePuzzle,
};
