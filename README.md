<img src="https://www.northern.co/wp-content/themes/northern/images/northern-pride-logo.svg" alt="Northern.co Logo" width="215">

# Northern.co puzzle

This repository contains the source code for the application that attempts to solve te puzzle found on [northern.co/wp-content/themes/northern/images/puzzle.png](https://www.northern.co/wp-content/themes/northern/images/puzzle.png).

@Northern Let me know your thoughts :blush: 

## Setup
To clone the contents of this repository, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Install Yarn (the package manager that I used)
$ npm install -g yarn

# Clone the repo
$ git clone git@gitlab.com:mitchdorrestijn/northern.co-puzzle.git

# Go into the northern.co-puzzle directory
$ cd northern.co-puzzle

# Install dependencies
$ yarn install
```

## Commands
The commands listed below can be ran in order to interact with the application.

```bash
# Runs the application and prints the solution in the console
$ yarn start

# Runs the tests
$ yarn test

# Runs the tests and displays the coverage
$ yarn test:coverage

# Tests the application for any structure or styling errors
$ yarn lint

# Attempts to fix any structure or styling errors
$ yarn lint:fix 

# Generate the documentation for the functions
$ yarn doc 
```

## Packages
The application uses code from several open source packages. These packages are listed below.

| Package                   | Reason                                                 | DevDependency |
|---------------------------|--------------------------------------------------------|---------------|
| [jimp](https://www.npmjs.com/package/jimp)                      | In order to read remote images                         | No            |
| [chai](https://www.npmjs.com/package/chai)                      | In order to write readable tests for this application  | Yes           |
| [eslint](https://www.npmjs.com/package/eslint)                    | In order to structure the application                  | Yes           |
| [eslint-config-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base) | In order to use the AirBnb styleguide for Eslint       | Yes           |
| [eslint-plugin-import](https://www.npmjs.com/package/eslint-plugin-import)      | In order to configure Eslint for this application      | Yes           |
| [jsdoc](https://www.npmjs.com/package/jsdoc)                     | In order to document the functions in this application | Yes           |
| [mocha](https://www.npmjs.com/package/mocha)                     | In order to run tests for this application             | Yes           |
| [nyc](https://www.npmjs.com/package/nyc)                       | In order to get code coverage from the tests           | Yes           |


## Author
* [Mitch Dorrestijn](https://nl.linkedin.com/in/mitchdorrestijn)